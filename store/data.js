import {AXSvc} from '../service/api'
var convert = require('xml-js');

export const state = () => ({
  sales             : [],
  taxNumbers        : [],
  fetching          : false,
  customerGroups    : [],
  termOfPayments    : [],
  methodOfPayments  : [],
  deliveryModes     : [],
  taxGroups         : [],
  prefix            : [],
})

export const mutations = {
  setSales(state, sales) {
    state.sales = sales
  },
  setTaxNumbers(state, taxNumbers) {
    state.taxNumbers = taxNumbers
  },
  setFetchingOn(state) {
    state.fetching = true
  },
  setFetchingOff(state) {
    state.fetching = false
  },
  setCustomerGroups(state, customerGroups) {
    state.customerGroups = customerGroups
  },
  setTermOfPayments(state, termOfPayments) {
    state.termOfPayments = termOfPayments
  },
  setMethodOfPayments(state, methodOfPayments) {
    state.methodOfPayments = methodOfPayments
  },
  setDeliveryModes(state, deliveryModes) {
    console.log(deliveryModes)
    state.deliveryModes = deliveryModes
  },
  setTaxGroups(state, taxGroups) {
    state.taxGroups = taxGroups
  },
  setPrefix(state, prefix) {
    state.prefix = prefix
  }
}

const helper = {
  resolveData(res) {
    // conver xml to json
    let result = convert.xml2js(res.data, {compact: true, spaces: 0})
    let data = result.Response.Result.Entity
    // if no data
    if (parseInt(result.Response.Result.TotalCount._text) == parseInt(0)) {
      return []
    }

    // if single data
    if (data.field != undefined) {
      data = [data]
    }

    // adjust json result to proper list
    let list = []
    for (var i in data) {
      let x = {}

      // if single data value for get delivery mode
      if (data[i].field.length == undefined) {
        x[data[i].field._attributes.name] = data[i].field._attributes.value
      } else {
        for (var j in data[i].field) {
          x[data[i].field[j]._attributes.name] = data[i].field[j]._attributes.value
        }
      }
      list.push(x)
    }

    return list
  }
}

export const actions = {
  sales({commit}) {
    AXSvc.getSales().then((res) => {
      commit('setSales', helper.resolveData(res))
    })
  },
  taxNumbers({commit}, filter){
    commit('setTaxNumbers', [])
    commit('setFetchingOn')
    AXSvc.getTaxNumbers(filter).then((res) => {
      commit('setTaxNumbers', helper.resolveData(res))
    }).catch(() => {})
    .then(() => {
      commit('setFetchingOff')
    })
  },
  customerGroups({commit}) {
    AXSvc.getCustomerGroups().then((res) => {
      commit('setCustomerGroups', helper.resolveData(res))
    })
  },
  termOfPayments({commit}) {
    AXSvc.getTermOfPayments().then((res) => {
      commit('setTermOfPayments', helper.resolveData(res))
    })
  },
  methodOfPayments({commit}) {
    AXSvc.getMethodOfPayments().then((res) => {
      commit('setMethodOfPayments', helper.resolveData(res))
    })
  },
  deliveryModes({commit}) {
    AXSvc.getDeliveryModes().then((res) => {
      commit('setDeliveryModes', helper.resolveData(res))
    })
  },
  taxGroups({commit}) {
    AXSvc.getTaxGroups().then((res) => {
      commit('setTaxGroups', helper.resolveData(res))
    })
  },
  prefix({commit}) {
    AXSvc.getPrefix().then((res) => {
      commit('setPrefix', helper.resolveData(res))
    })
  },
  regions({commit}, filter) {
    return new Promise((resolve, reject) => {
      AXSvc.getRegions(filter).then((res) => {
        resolve(helper.resolveData(res))
      }).catch((error) => {
        reject(error)
      })
    })
  }
}