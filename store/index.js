export const state = () => ({
  counter: 0,
  collapsed: false,
  token: null,
  menu: '',
  menuOpen: [],
  routeName: ''
})

export const mutations = {
  increment(state) {
    state.counter++
  },
  toggle(state) {
    state.collapsed = !state.collapsed
  },
  setMenu(state, menu) {
    state.menu = menu
  },
  setMenuOpen(state, menuOpen) {
    state.menuOpen = menuOpen
  },
  setRouteName(state, routeName) {
    state.routeName = routeName
  }
}

export const getters = {
  collapsed(state) {
    return state.collapsed
  },
  token(state) {
    return state.token
  }
}

export const actions = {
  toggleSidebar({commit}) {
    commit('toggle')
  },
  openMenu({commit}, menu) {
    console.log()
    commit('setMenuOpen', menu)
  }
}
