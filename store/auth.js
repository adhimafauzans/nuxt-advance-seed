import {AuthSvc} from '../service/api'

export const state = () => ({
  token: null,
  user: {}
})

export const mutations = {
  setUser(state, user) {
    state.user = user
  },
  remove(state) {
    state.token = null
    state.user = {}
    localStorage.token = null
  },
  setToken(state, token) { state.token = token }
}

export const actions = {
  login({commit}, params) {
    return new Promise((resolve, reject) => {
      AuthSvc.login(params).then((res) => {
        commit('setToken', res.data.token)
        localStorage.token = res.data.token

        AuthSvc.getUserByToken().then((result) => {
          commit('setUser', result.data.data)
          resolve(result.data)
        }).catch((err) => {
          reject(err)
        })
      }).catch((error) => {
        reject(error)
      })
    })
  }
}

export const getters = {
  role (state) {
    return state.user.roles[0].name
  },
  roles (state) {
    return state.user.roles
  }
}
