export default function ({ redirect, route, store }) {
  let role = store.getters['auth/role']

  if (role != 'admin') {
    return redirect('/')
  }
}
