export default function ({ redirect, route, store }) {
  store.commit("setMenu", route.path.split("/")[1])
  store.commit("setRouteName", route.name)

  let token = localStorage.token
  if (token == null) {
    return redirect('/login')
  }
}
