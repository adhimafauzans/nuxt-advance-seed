export default function ({ redirect }) {
  let token = localStorage.token
  if (token) {
    return redirect('/')
  }
}
