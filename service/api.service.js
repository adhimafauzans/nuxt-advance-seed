import axios from 'axios';
const https = require('https');

export const Api = {
    init() {
        let service = axios.create({
            baseURL: process.env.API_URL,
            headers: {
              // 'Content-Type': 'multipart/form-data'
              // 'Platform-ID': process.env.PLATFORM_ID,
              // 'Api-Key': process.env.API_KEY_USER,
            },
            //ignore SSL
            httpsAgent: new https.Agent({  
              rejectUnauthorized: false
            })
        });

        service.interceptors.request.use(
            config => {
                var token = window.localStorage.getItem("token");

                if(token != null){
                    config.headers['Authorization'] = 'Bearer ' + token;
                }
                return config;
            },
            error => Promise.reject(error)
        );

        service.interceptors.response.use(this.handleSuccess, this.handleError);
        this.service = service;
    },

    handleSuccess(response) {
        return response;
    },

    handleError(error) {
        // switch (error.response.status) {
        //     case 401:
        //         this.redirectTo(document, '/')
        //         break;
        //     case 404:
        //         this.redirectTo(document, '/404')
        //         break;
        //     default:
        //         this.redirectTo(document, '/500')
        //         break;
        // }
        return Promise.reject(error.response.data.errors[0])
    },

    redirectTo(document, path) {
        document.location = path
    },

    get(path) {
        return this.service.get(path)
    },

    put(path, payload) {
        return this.service.request({
            method: 'PUT',
            url: path,
            responseType: 'json',
            data: payload
        })
    },

    post(path, payload) {
        return this.service.request({
            method: 'POST',
            url: path,
            responseType: 'json',
            data: payload
        });
    },

    patch(path, payload) {
        return this.service.request({
            method: 'PATCH',
            url: path,
            responseType: 'json',
            data: payload
        });
    },
    delete(path, payload) {
        return this.service.request({
            method: 'DELETE',
            url: path,
            responseType: 'json',
            data: payload
        });
    },
    generateFormData(payload) {
      let formData = new FormData();
      for (var key in payload) {
        if (payload[key] != undefined) {
          if (payload[key].constructor === Array) {
            for (var i in payload[key]) {
              formData.append(`${key}[${i}]`, payload[key][i]);
            }
          } else {
            formData.append(key, payload[key]);
          }
        }
      }
      return formData;
    }
}

export const ApiUser = {
    init() {
        let service = axios.create({
            baseURL: process.env.API_URL_USER,
            headers: {
              'Platform-ID': process.env.PLATFORM_ID,
              'Api-Key': process.env.API_KEY_USER,
            },
            httpsAgent: new https.Agent({  
              rejectUnauthorized: false
            })
        });

        service.interceptors.request.use(
            config => {
                var token = window.localStorage.getItem("token");

                if(token != null){
                    config.headers['Authorization'] = 'Bearer ' + token;
                }
                return config;
            },
            error => Promise.reject(error)
        );

        service.interceptors.response.use(this.handleSuccess, this.handleError);
        this.service = service;
    },

    handleSuccess(response) {
        return response;
    },

    handleError(error) {
        // switch (error.response.status) {
        //     case 401:
        //         this.redirectTo(document, '/')
        //         break;
        //     case 404:
        //         this.redirectTo(document, '/404')
        //         break;
        //     default:
        //         this.redirectTo(document, '/500')
        //         break;
        // }
        return Promise.reject(error.response.data.errors[0])
    },

    redirectTo(document, path) {
        document.location = path
    },

    get(path) {
        return this.service.get(path)
    },

    put(path, payload) {
        return this.service.request({
            method: 'PUT',
            url: path,
            responseType: 'json',
            data: payload
        })
    },

    post(path, payload) {
        return this.service.request({
            method: 'POST',
            url: path,
            responseType: 'json',
            data: payload
        });
    },

    patch(path, payload) {
        return this.service.request({
            method: 'PATCH',
            url: path,
            responseType: 'json',
            data: payload
        });
    },
    delete(path, payload) {
        return this.service.request({
            method: 'DELETE',
            url: path,
            responseType: 'json',
            data: payload
        });
    }
}


export const ApiAX = {
  init() {
      let service = axios.create({
          baseURL: process.env.API_URL_AX,
          headers: {
            'Content-Type': 'application/xml',
            'X-ApiKey'    : process.env.API_KEY_AX
          }
      });

      service.interceptors.response.use(this.handleSuccess, this.handleError);
      this.service = service;
  },

  handleSuccess(response) {
      return response;
  },

  handleError(error) {
      // switch (error.response.status) {
      //     case 401:
      //         this.redirectTo(document, '/')
      //         break;
      //     case 404:
      //         this.redirectTo(document, '/404')
      //         break;
      //     default:
      //         this.redirectTo(document, '/500')
      //         break;
      // }
      return Promise.reject(error)
  },

  redirectTo(document, path) {
      document.location = path
  },

  get(path) {
      return this.service.get(path)
  },

  put(path, payload) {
      return this.service.request({
          method: 'PUT',
          url: path,
          // responseType: 'json',
          data: payload
      })
  },

  post(path, payload) {
      return this.service.request({
          method: 'POST',
          url: path,
          // responseType: 'json',
          data: payload
      });
  },

  patch(path, payload) {
      return this.service.request({
          method: 'PATCH',
          url: path,
          // responseType: 'json',
          data: payload
      });
  },
  delete(path, payload) {
      return this.service.request({
          method: 'DELETE',
          url: path,
          // responseType: 'json',
          data: payload
      });
  },
}

export const ApiService = {
    init(){
        Api.init();
        ApiAX.init();
        ApiUser.init();
    }
}


