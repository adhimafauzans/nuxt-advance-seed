import {Api, ApiAX, ApiUser} from '@/service/api.service'

export const AuthSvc = {
  login(params) {
    return ApiUser.post('auth/login', params)
  },
  getUserByToken() {
    return ApiUser.get('me')
  },
  changePassword(params) {
    return ApiUser.post('auth/change-password', params)
  }
}

export const UserSvc = {
  getList(page) {
    return ApiUser.get(`users?page=${page}`)
  },
  getDetail(id) {
    return ApiUser.get(`users/${id}`)
  },
  insert(params) {
    return ApiUser.post('users', params)
  },
  update(id, params) {
    return ApiUser.put(`users/${id}`, params)
  },
  delete(id) {
    return ApiUser.delete(`users/${id}`)
  },
}

export const CustomerSvc = {
  getList(page, perpage, filter) {
    return Api.get(`customers?page=${page}&perpage=${perpage}&name=${filter.name}&status=${filter.status}`)
  },
  getDetail(id) {
    return Api.get(`customers/${id}`)
  },
  insert(params) {
    return Api.post('customers', params)
  },
  update(id, params) {
    return Api.post(`customers/${id}`, params)
  },
  delete(id) {
    return Api.delete(`customers/${id}`)
  },
  updateStatus(id, status) {
    return Api.post(`/customers/${id}/change-statuses`, {status})
  },
  approve(id) {
    return Api.post(`/customers/${id}/sends`)
  }
}

export const AXSvc = {
  getCustomerGroups() {
    let query = `<?xml version="1.0"?>
      <Request>
        <Operation>GetEntities</Operation>
        <Params>
          <TABLE>CustGroup</TABLE>
          <PageIndex>0</PageIndex>
          <PageSize>50</PageSize>
          <SortField></SortField>
          <SortAscending>1</SortAscending>
          <ExtraFields>
            <field name="Custgroup"/>
            <field name="NAME"/>
          </ExtraFields>
        </Params>
      </Request>`

    return ApiAX.post('', query)
  },
  getSales() {
    let query = `<?xml version="1.0"?>
      <Request>
          <Operation>GetEntities</Operation>
          <Params>
              <Table>AGIT_WORKERVIEW</Table>
              <PageIndex>0</PageIndex>
              <PageSize>1000</PageSize>
              <SortField></SortField>
              <SortAscending>1</SortAscending>
              <ExtraFields>
                  <field name="PERSON"/>
                  <field name="PERSONNELNUMBER"/>
                  <field name="NAME"/>
              </ExtraFields>
              <WebsiteId>SanaStore</WebsiteId>
          </Params>
      </Request>`

    return ApiAX.post('', query)
  },
  getTaxNumbers(filter) {
    let filterQuery = ''
    if (filter) {
      if (isNaN(filter)) {
        //is not a number
        filterQuery = `<for field="NAME">
          <CONTAINS>${filter}</CONTAINS>
        </for>`
      } else {
        filterQuery = `<for field="VATNUM">
          <CONTAINS>${filter}</CONTAINS>
        </for>`
      }
    }

    let query = `<?xml version="1.0"?>
      <Request>
        <Operation>GetEntities</Operation>
        <Params>
          <TABLE>TAXVATNUMTABLE</TABLE>
          <PageIndex>0</PageIndex>
          <PageSize>20</PageSize>
          <SortField></SortField>
          <SortAscending>1</SortAscending>
          <Filter>
            ${filterQuery}
          </Filter>
          <ExtraFields>
            <field name="VATNUM"/>
            <field name="NAME"/>
          </ExtraFields>
          <WebsiteId>SanaStore</WebsiteId>
        </Params>
      </Request>`

    return ApiAX.post('', query)
  },
  getTermOfPayments() {
    let query = `<?xml version="1.0"?>
      <Request>
        <Operation>GetEntities</Operation>
        <Params>
          <TABLE>PaymTerm</TABLE>
          <PageIndex>0</PageIndex>
          <PageSize>50</PageSize>
          <SortField></SortField>
          <SortAscending>1</SortAscending>
          <ExtraFields>
            <field name="PaymTermId"/>
            <field name="Description"/>
          </ExtraFields>
        </Params>
      </Request>`

    return ApiAX.post('', query)
  },
  getMethodOfPayments() {
    let query = `<?xml version="1.0"?>
      <Request>
        <Operation>GetEntities</Operation>
        <Params>
          <TABLE>CustPaymModeTable</TABLE>
          <PageIndex>0</PageIndex>
          <PageSize>50</PageSize>
          <SortField></SortField>
          <SortAscending>1</SortAscending>
          <ExtraFields>
            <field name="Paymmode"/>
            <field name="Name"/>
          </ExtraFields>
        </Params>
      </Request>`

    return ApiAX.post('', query)
  },
  getDeliveryModes() {
    let query = `<?xml version="1.0"?>
      <Request>
        <Operation>GetEntities</Operation>
        <Params>
          <TABLE>DlvMode</TABLE>
          <PageIndex>0</PageIndex>
          <PageSize>50</PageSize>
          <SortField></SortField>
          <SortAscending>1</SortAscending>
          <ExtraFields>
            <field name="Code"/>
          </ExtraFields>
        </Params>
      </Request>`
      
    return ApiAX.post('', query)
  },
  getTaxGroups() {
    let query = `<?xml version="1.0"?>
      <Request>
        <Operation>GetEntities</Operation>
        <Params>
          <TABLE>Agit_TaxGroupCustomer</TABLE>
          <PageIndex>0</PageIndex>
          <PageSize>50</PageSize>
          <SortField></SortField>
          <SortAscending>1</SortAscending>
          <Filter>
            <for field="TaxGroup">
              <Equals></Equals>
            </for>
          </Filter>
          <ExtraFields>
            <field name="TaxGroup"/>
            <field name="TaxGroupName"/>
          </ExtraFields>
        </Params>
      </Request>`
      
    return ApiAX.post('', query)
  },
  getPrefix() {
    let query = `<?xml version="1.0"?>
      <Request>
        <Operation>GetEntities</Operation>
        <Params>
          <TABLE>KRE_MasterPrefiks</TABLE>
          <PageIndex>0</PageIndex>
          <PageSize>50</PageSize>
          <SortField></SortField>
          <SortAscending>1</SortAscending>
          <ExtraFields>
            <field name="KRE_ID_Tax_Prefiks"/>
            <field name="TaxGroup"/>
            <field name="Description"/>
          </ExtraFields>
        </Params>
      </Request>`
      
    return ApiAX.post('', query)
  },
  getRegions(filter) {
    let filterQuery = ''      
    if (filter.zipcode) {
      filterQuery += `<for field="ZipCode">
        <CONTAINS>${filter.zipcode}</CONTAINS>
      </for>`
    }

    if (filter.city) {
      filterQuery += `<for field="City">
        <CONTAINS>${filter.city}</CONTAINS>
      </for>`
    }

    if (filter.state) {
      filterQuery += `<for field="State">
        <CONTAINS>${filter.state}</CONTAINS>
      </for>`
    }

    if (filter.district) {
      filterQuery += `<for field="District">
        <CONTAINS>${filter.district}</CONTAINS>
      </for>`
    } 
  


    let query = `<?xml version="1.0"?>
      <Request>
        <Operation>GetEntities</Operation>
        <Params>
          <TABLE>LogisticsAddressZipCode</TABLE>
          <PageIndex>0</PageIndex>
          <PageSize>20</PageSize>
          <SortField></SortField>
          <SortAscending>1</SortAscending>
          <Filter>
            ${filterQuery}
          </Filter>
          <ExtraFields>
            <field name="ZipCode"/>
            <field name="City"/>
            <field name="State"/>
            <field name="District"/>
          </ExtraFields>
        </Params>
      </Request>`

    return ApiAX.post('', query)
  },
  insertSales(params) {
    let parameters = {
      first_name  : params.firstName,
      middle_name : params.middleName,
      last_name   : params.lastName,
    }

    return Api.post('sales-takers', parameters)
  },
  insertTaxExemptNumber(params) {
    let parameters = {
      country_region_id : 'IDN',
      vat_num           : params.NPWPNumber,
      company_name      : params.companyName,
      company_address   : params.companyAddress
    }

    return Api.post('npwps', parameters)
  }
}