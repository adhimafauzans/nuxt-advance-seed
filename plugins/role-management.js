import Vue from 'vue'

let roles = {
  'menu-customer'   : ['admin', 'finance', 'marketing', 'sales'],
  'menu-user'       : ['admin'],
  'menu-log'        : ['admin'],
  'menu-inbox'      : ['admin'],
  'new-customer'    : ['admin', 'sales'],
  'add-contact'     : ['admin', 'sales'],
  'add-address'     : ['admin', 'sales'],
  'edit-contact'    : ['admin', 'sales'],
  'edit-address'    : ['admin', 'sales'],
  'request-approval': ['admin', 'sales', 'marketing'],
  'approved'        : ['admin', 'marketing', 'finance'],
  'draft-action'    : ['admin', 'sales'],
  'reject-action'   : ['admin', 'marketing', 'finance'],
  'tab-draft'       : ['sales'],
  'tab-request'     : ['marketing', 'finance'],
  'tab-rejected'    : ['sales'],
  'filter-draft'    : ['admin', 'sales']
}

Vue.mixin({
	methods: {
    checkAccess (access) {
      var result = roles[access].indexOf(this.role)

      return result > -1
    }
  }
})
