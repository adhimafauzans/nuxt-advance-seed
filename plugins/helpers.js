import Vue from 'vue'


Vue.mixin({
	mounted() {
  	$('form input').on('keydown', function(e){
      let selector = e.target.id || null
      if (selector && $(`#${selector}`).hasClass('invalid-field')) {
        $(`#${selector}`).removeClass('invalid-field')
        $(`#${selector} + .invalid-text`).text('')
      }
    })
  },
	methods: {
    drawErrorField(selector, message) {
    	let errorTextExist = $(`#${selector}`).siblings(".invalid-text").length
    	if (!errorTextExist) {
    		$(`#${selector}`).after( "<small class='invalid-text'></small>" );
    	}

      $(`#${selector}`).addClass('invalid-field')
			$(`#${selector} + .invalid-text`).text(message)
      $('html,body').animate({scrollTop: $(`#${selector}`).offset().top - 70});
		},
		clearError() {
			$('form input').removeClass('invalid-field')
      $('.invalid-text').text('')
		}
  },
})

